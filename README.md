# tacon-jakarta


```mermaid
flowchart TD
    subgraph tacon-jakarta-validation
        direction RL
        jakarta.validation
    end

    subgraph tacon-jakarta-persistence
        direction RL
        AbstractJpaEntity
        EntityUtils
    end
    
    subgraph tacon-jakarta-query
        direction RL
        jakarta.persistence
        IQuery...
        IExpression...
    end

    subgraph tacon-jakarta-interfaces
        direction RL
        jakarta.ejb
        EjbManagerException
        EjbValidationManagerException
        BeanValidator
    end

    subgraph tacon-jakarta-ejb
        direction RL
        jakarta.ejb
        Manager*
    end

tacon-jakarta-ejb --> tacon-jakarta-interfaces --> tacon-jakarta-validation
tacon-jakarta-ejb --> tacon-jakarta-persistence
tacon-jakarta-ejb --> tacon-jakarta-query
```
